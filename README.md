# du-vent-et-du-sable

Projeto com codigo-fonte, scripts, samples, poesias, fotos, ideias e tudo o
mais sobre a performance "Du vent et du sable: une transposition de dune à
Paris", realizada em 1 de Maio de 2022 na cidade de Paris.

## Sobre a performance

Projeto idealizado por Mari Moura em colaboracao com Joenio M. Costa...

## Codigo-fonte `main.hrung`

Este é o arquivo principal com a producao dos sons e musicas que acompanham a
performance arte, o formato `hrung` é suportado pela ferramenta
[Hrung][hrung] (ainda em desenvolvimento em versão alpha).

## Samples

Alguns samples utilizados na performance foram obtidos em
[Freesound.org][freesound] (projeto colaborativo para compartilhamento de sons
com licencas livres creative commons), segue abaixo as referencias utilizadas:

* [Wind, Synthesized, A.wav](https://freesound.org/people/InspectorJ/sounds/376415), _som de vento bom para usar de fundo_.
* [Wind » Medium Wind](https://freesound.org/people/kangaroovindaloo/sounds/205966), _vento_
* [Wind Chime, Tolling, A.wav](https://freesound.org/people/InspectorJ/sounds/411729), _sino_.
* [Wind Chime, Gamelan Gong, A.wav](https://freesound.org/people/InspectorJ/sounds/411090), _outro sino, mais grave_.
* [RAM_Mouth Hawk_rev_v1.wav](https://freesound.org/people/reidedo/sounds/344445), _aguia_.
* [ Hawk/Eagle Cry (Distant) ](https://freesound.org/people/PRINCEofWORMS/sounds/571273), _aguia_.

## Video fragments

Trecho do filme Dune (1984) usado junto ao software Le Biniou em combinacao com
os visuais e live coding da performance.

## Poesia / Poésie en français

Os arquivos [POESIE_1.txt](./POESIE_1.txt) e [POESIE_2.txt](./POESIE_2.txt)
contem poesias compostas por Mari Moura durante a sua atuacao intensa como
primeira Ministra do Estado Sensivel na realizacao desta performance.

### Audio /  Poésie en français

Alguns trechos das poesias foram sintetizadas com [eSpeak NG
Text-to-Speech][espeak-ng] (um software livre para sintese de fala), abaixo
exemplos dos parametros utilizados para gerar os audios.

```sh
espeak-ng -v fr-fr -s 150 "Du vent" -w espeak.wav
```

## Autores

* Joenio Marques da Costa <joenio@joenio.me>
* Mari Moura <marimoura5@hotmail.com>

## Licenca

![Licença Creative Commons](https://i.creativecommons.org/l/by-sa/4.0/80x15.png)<br/>
Este obra está licenciado com uma Licença [Creative Commons Atribuição-CompartilhaIgual 4.0 Internacional][by-sa-4.0].

## Copyright

Copyright © Estado Sensivel

[freesound]: https://freesound.org
[espeak-ng]: https://github.com/espeak-ng/espeak-ng/
[by-sa-4.0]: http://creativecommons.org/licenses/by-sa/4.0/
[hrung]: https://codeberg.org/joenio/hrung
